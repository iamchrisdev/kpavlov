import { Router } from 'preact-router';

import Header from './header';
// Code-splitting is automated for routes
import Home from '../routes/home';
import Auth from '../routes/login';
import { Container } from '@material-ui/core';
import React from 'preact/compat';

export default function App() {
	const token = localStorage.getItem( 'token' );

	if (!token) {
		return <Auth path="/login" />;
	}

	// /** Gets fired when the route changes.
	//  *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	//  *	@param {string} event.url	The newly routed URL
	//  */
	const handleRoute = e => {
		console.log(e);
	};

	return (
		<div id="app">
			<Header />
			<Container component="main" maxWidth="lg">
				<Router onChange={ handleRoute }>
					<Home default path="/" />
				</Router>
			</Container>
		</div>
	);
}
