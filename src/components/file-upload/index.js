/** @jsx h */
import { h } from 'preact';
import { useState } from 'preact/hooks';
import { FilePond, registerPlugin } from 'react-filepond';

import { fileUpload } from '../../services/file.service';

import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import 'filepond/dist/filepond.min.css';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// Register the plugins
registerPlugin( FilePondPluginImagePreview );

const useStyles = makeStyles( theme => ({
	modal: {
		minWidth: 400,
		minHeight: 400,
		display: 'grid',
		alignItems: 'center'
	},
	submit: {
		margin: theme.spacing( 5, 0, 2 ),
		border: 0,
		borderRadius: 3,
		height: 48,
		padding: '0 30px'
	}
}) );

const FileUpload = ({ handleClose }) => {
	let filePondInstance;
	const classes = useStyles();

	const [ files, setFiles ] = useState( [] );

	const handleSubmit = async () => {
		if (files.length > 0) {
			//TODO: Handle res
			const res = await fileUpload( files );

			//TODO: Call this only when file upload is successful
			handleClose( files[0] );
		}
	};

	return (
		<div class={ classes.modal }>
			<FilePond
				ref={ ref => filePondInstance = ref }
				files={ files }
				allowMultiple={ true }
				maxFiles={ 1 }
				onupdatefiles={ fileItems => setFiles( fileItems.map( fileItem => fileItem.file ) ) } />

			<Button
				fullWidth
				variant="contained"
				color="primary"
				class={ classes.submit }
				disabled={ files.length <= 0 }
				onClick={ handleSubmit }>
				Upload
			  </Button>
		</div>
	);
};

export default FileUpload;