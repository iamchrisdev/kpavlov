/** @jsx h */
import { h } from 'preact';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles( theme => ({
	root: {
		flexGrow: 1
	},
	menuButton: {
		marginRight: theme.spacing( 2 )
	},
	bar: {
		backgroundColor: theme.palette.primary.dark
	},
	title: {
		flexGrow: 1
	}
}) );

const Header = () => {
	const classes = useStyles();
	const logout = () => {
		localStorage.removeItem( 'token' );
		window.location = '/login';
	};

	return (
		<div className={ classes.root }>
			<AppBar position="static">
				<Toolbar className={ classes.bar}>
				  <Typography variant="h6" className={ classes.title }>
					kpavlov
				  </Typography>
				  <Button color="inherit" onClick={ logout }>Logout</Button>
				</Toolbar>
			</AppBar>
		</div>
	);
};

export default Header;
