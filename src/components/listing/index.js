/** @jsx h */
import { h } from 'preact';
import { useEffect, useState } from 'preact/hooks';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles( {
	root: {
		width: '100%',
		overflowX: 'auto'
	},
	table: {
		minWidth: 650
	}
} );

function createData(name, type, size) {
	return { name, type, size };
}

function bytesToSize(bytes) {
	if (bytes === 0) return '0 Byte';

	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

const Listing = ({ fileList }) => {
	const classes = useStyles();
	const [ filesList, setFiles ] = useState( [] );

	useEffect( () => {
		const files = fileList.map( file => createData( file.name, file.type, file.size ) );

		setFiles( [ ...files ] );
	}, [ filesList ] );

	return (
		<Paper className={ classes.root }>
		  <Table className={ classes.table } aria-label="simple table">
			<TableHead>
			  <TableRow>
				<TableCell>File Name</TableCell>
				<TableCell align="right">Type</TableCell>
				<TableCell align="right">Size</TableCell>
			  </TableRow>
			</TableHead>
			<TableBody>
			  {
				  filesList.length > 0 ?
					  filesList.map( (file, index) => (
						  <TableRow key={ index }>
						  <TableCell component="th" scope="row">
							{ file.name }
						  </TableCell>
						  <TableCell align="right">{ file.type }</TableCell>
						  <TableCell align="right">{ bytesToSize(file.size) }</TableCell>
						</TableRow>
					  ) ) :
					  <TableRow>
						<TableCell colSpan={ 3 } style={ { textAlign: 'center' } }> No files to show </TableCell>
					  </TableRow>
			  }
			</TableBody>
		  </Table>
		</Paper>
	);
};

export default Listing;