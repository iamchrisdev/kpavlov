/** @jsx h */
import { h } from 'preact';
import { useEffect, useState } from 'preact/hooks';

import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import Listing from '../../components/listing';
import FileUpload from '../../components/file-upload';
import { fetchFiles } from '../../services/file.service';

const useStyles = makeStyles( theme => ({
	container: {
		paddingTop: '40px'
	},
	actions: {
		display: 'grid',
		justifyItems: 'end'
	},
	actionButton: {
		marginBottom: '20px'
	},
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		padding: theme.spacing( 2, 4, 3 )
	}
}) );

const Home = () => {
	const classes = useStyles();

	const [ open, setOpen ] = useState( false );
	const [ fileList, setFiles ] = useState( [] );

	useEffect( async () => {
		const res = await fetchFiles();

		if (res && res.data) {
			setFiles( res.data.files );
		}
	}, [] );

	const handleOpen = () => {
		setOpen( true );
	};

	const handleClose = file => {
		setFiles( [ ...fileList, file ] );
		setOpen( false );
	};

	return (
		<div className={ classes.container }>
			<div className={ classes.actions }>
				<Button
					className={ classes.actionButton }
					variant="contained"
					color="primary"
					onClick={ handleOpen }>
					Upload File
				</Button>
			</div>

			     <Modal
					 aria-labelledby="simple-modal-title"
					 aria-describedby="simple-modal-description"
					 className={ classes.modal }
					 open={ open }
					 onClose={ handleClose }>
					<div className={ classes.paper }>
					  <h2 id="simple-modal-title">File upload</h2>
					  <FileUpload handleClose={ handleClose } />
					</div>
     			 </Modal>

			<Listing fileList={ fileList } />
		</div>
	);
};


export default Home;
