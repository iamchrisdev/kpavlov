/** @jsx h */
import { h } from 'preact';
import { useState } from 'preact/hooks';
import axios from 'axios';

import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

function Copyright() {
	return (
		<Typography variant="body2" color="textSecondary" align="center">
		  { 'Copyright © ' }
			{ new Date().getFullYear() }
			{ '.' }
		</Typography>
	);
}

const useStyles = makeStyles( theme => ({
	'@global': {
		body: {
			backgroundColor: '#F2F2F2',
			height: '100vh'
		}
	},
	container: {
		display: 'grid',
		alignItems: 'center',
		height: '100%'
	},
	paper: {
		marginTop: theme.spacing( 8 ),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		backgroundColor: theme.palette.common.white,
		padding: '60px 40px',
		borderRadius: '8px',
		boxShadow: '0 0 15px #E0E0E0'
	},
	form: {
		width: '100%',
		marginTop: theme.spacing( 3 )
	},
	option: {
		marginTop: 15
	},
	link: {
		margin: theme.spacing( 1 )
	},
	submit: {
		margin: theme.spacing( 5, 0, 2 ),
		border: 0,
		borderRadius: 3,
		height: 48,
		padding: '0 30px'
	}
}) );

const VIEW = {
	LOGIN: 'LOGIN',
	REGISTRATION: 'REGISTRATION'
};

//TODO: Set correct endpoints
const ENDPOINT = {
	LOGIN: 'https://reqres.in/api/login',
	REGISTRATION: 'https://reqres.in/api/users'
};

const Auth = props => {
	const classes = useStyles();

	const [ view, setView ] = useState( 'LOGIN' );
	const [ isFormValid, setFormValid ] = useState( true );

	//TODO: Clear initial values
	const [ formValue, setValue ] = useState( {
		email: 'eve.holt@reqres.in',
		password: 'cityslicka'
	} );

	const submit = async () => {
		const url = view === VIEW.LOGIN ? ENDPOINT.LOGIN : ENDPOINT.REGISTRATION;

		try {
			const res = await axios.post( url, { ...formValue } );

			if (res.data.token) {
				localStorage.setItem( 'token', res.data.token );
				window.location = '/';
			}
		} catch (e) {
			console.error( e );
		}
	};

	const preventDefault = (event, view) => {
		event.preventDefault();
		setView( view );
	};

	const checkPasswordMatch = value => {
		if (value.trim() !== formValue.password.trim()) {
			setFormValid( false );
		}
		else {
			setFormValid( true );
		}

		preventDefault();
	};

	const handlePasswordTyping = value => {
		setValue( { ...formValue, password: value } );
		setFormValid( false );
	};

	return (
		<Container component="main" maxWidth="xs" class={ classes.container }>
		  <CssBaseline />
		  <div class={ classes.paper }>
			<Typography component="h1" variant="h5">
			 {
				 view === VIEW.LOGIN ? 'Sign in' : 'Registration'
			 }
			</Typography>
			<form class={ classes.form } noValidate autoComplete="off">
			  <TextField
				  variant="standard"
				  margin="normal"
				  required
				  fullWidth
				  id="email"
				  label="Email Address"
				  name="email"
				  autoComplete="email"
				  autoFocus
				  defaultValue={ formValue.email }
				  onChange={ e => setValue( { ...formValue, email: e.target.value } ) } />
			  <TextField
				  variant="standard"
				  margin="normal"
				  required
				  fullWidth
				  name="password"
				  label="Password"
				  type="password"
				  id="password"
				  defaultValue={ formValue.password }
				  autoComplete="current-password"
				  onChange={ e => handlePasswordTyping( e.target.value ) } />
				{
					view === VIEW.REGISTRATION
						? (
							<TextField
								error={ !isFormValid }
								helperText={ isFormValid ? '' : 'Passwords don\'t match.' }
								variant="standard"
								margin="normal"
								required
								fullWidth
								name="password"
								label="Repeat Password"
								type="password"
								id="password"
								defaultValue={ '' }
								onChange={ e => checkPasswordMatch( e.target.value ) }
							/>
						)
						: null
				}

				<Button
					fullWidth
					variant="contained"
					color="primary"
					class={ classes.submit }
					onClick={ submit }>
					{ view === VIEW.LOGIN ? 'Sign in' : 'Sign Up' }
				</Button>

			</form>
			  <div className={ classes.option }>
				  {
					  view === VIEW.LOGIN

						  ? (
							  <Typography component="p">
							  Don't have an account?
							  <Link href="#"
									onClick={ e => preventDefault( e, VIEW.REGISTRATION ) }
									className={ classes.link }>
								Sign Up
							  </Link>
						  </Typography>
						  )
						  : (
							  <Typography component="p">
							  Already have account?
							  <Link href="#"
									onClick={ e => preventDefault( e, VIEW.LOGIN ) }
									className={ classes.link }>
								Sign In
							  </Link>
						  </Typography>
						  )
				  }
			  </div>
		  </div>
		  <Box mt={ 8 }>
			<Copyright />
		  </Box>
    	</Container>
	);
};

export default Auth;