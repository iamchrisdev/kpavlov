import axios from 'axios';

export const fileUpload = async (files) => {
	const formData = new FormData();
	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	};

	formData.append('file', files[0], files[0].name);

	try {
		return await axios.post( `/api/files`, formData, config );
	} catch (e) {
		console.error(e);
	}
};

export const fetchFiles = async () => {
	try {
		const res = await axios.get( `/api/files`, { data: null } );

		return res.data.files;
	} catch (e) {
		console.error(e);
	}
};